package br.com.inmetrics.teste.website.test;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import java.io.File;
import org.openqa.selenium.chrome.ChromeDriver;
import br.com.inmetrics.teste.website.pageobjects.AddNewEmployeePage;
import br.com.inmetrics.teste.website.pageobjects.LoginPage;
import br.com.inmetrics.teste.website.pageobjects.MainPage;
import br.com.inmetrics.teste.website.pageobjects.SignUpPage;

public class Hooks {
    BaseSetup baseSetup;

    public Hooks(BaseSetup baseSetup) {
        this.baseSetup = baseSetup;
    }

    @Before("@website")
    public void setup() {
        String os = System.getProperty("os.name").toLowerCase();

        if(os.contains("mac")) {
            File file = new File("src/test/resources/driver/chromedriver_mac");
            System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
        } else if(os.contains("linux")) {
            File file = new File("src/test/resources/driver/chromedriver_linux");
            System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
        };

        baseSetup.driver = new ChromeDriver();
        baseSetup.driver.get("http://www.inmrobo.tk/accounts/login/");
        baseSetup.driver.manage().window().maximize();

        baseSetup.loginPage = new LoginPage(baseSetup.driver);
        baseSetup.signUpPage = new SignUpPage(baseSetup.driver);
        baseSetup.mainPage = new MainPage(baseSetup.driver);
        baseSetup.addNewEmployeePage = new AddNewEmployeePage(baseSetup.driver);
    }

    @After("@website")
    public void teardown() {
        if(baseSetup.driver != null) baseSetup.driver.quit();
    }
}