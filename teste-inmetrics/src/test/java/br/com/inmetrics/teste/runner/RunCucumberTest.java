package br.com.inmetrics.teste.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = {"br.com.inmetrics.teste.website.test", "br.com.inmetrics.teste.webservice.test"},
        plugin = {"pretty"},
        tags = "@webservice or @website",
        publish = true)
public class RunCucumberTest { }
