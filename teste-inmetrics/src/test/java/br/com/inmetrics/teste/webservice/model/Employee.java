package br.com.inmetrics.teste.webservice.model;

public class Employee {
    String admissao;
    String cargo;
    String comissao;
    String cpf;
    int departamentoId;
    String nome;
    String salario;
    String sexo;
    String tipoContratacao;

    public Employee(String admissao, String cargo, String comissao, String cpf, int departamentoId,
                    String nome, String salario, String sexo, String tipoContratacao) {
        this.admissao = admissao;
        this.cargo = cargo;
        this.comissao = comissao;
        this.cpf = cpf;
        this.departamentoId = departamentoId;
        this.nome = nome;
        this.salario = salario;
        this.sexo = sexo;
        this.tipoContratacao = tipoContratacao;
    }

    public String getAdmissao() {
        return admissao;
    }

    public void setAdmissao(String admissao) {
        this.admissao = admissao;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getComissao() {
        return comissao;
    }

    public void setComissao(String comissao) {
        this.comissao = comissao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(int departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSalario() {
        return salario;
    }

    public void setSalario(String salario) {
        this.salario = salario;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(String tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }
}
