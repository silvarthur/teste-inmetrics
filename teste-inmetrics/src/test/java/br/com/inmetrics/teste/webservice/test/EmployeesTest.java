package br.com.inmetrics.teste.webservice.test;

import br.com.inmetrics.teste.webservice.Endpoint;
import br.com.inmetrics.teste.webservice.model.Employee;
import br.com.inmetrics.teste.webservice.util.Request;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import java.util.HashMap;
import java.util.Map;

public class EmployeesTest {
    Response response;

    @Given("A GET request is sent to the server without specifying an employee")
    public void getAllEmployees() {
        response = Request.doGetRequest(Endpoint.LIST_ALL_EMPLOYEES);
    }

    @Then("A list containing all the employee must be returned")
    public void assertTheListOfAllEmployeesWasReturned() {
        Assert.assertEquals(HttpStatus.SC_OK, response.getStatusCode());
    }

    @Given("A POST request is sent to server to create a new employee")
    public void createNewEmployee() {
        Employee newEmployee = new Employee(
                "06/11/2020",
                "Analista de Qualidade",
                "0,00",
                "012.345.789-01",
                6,
                "Teste Um",
                "5.000,00",
                "m",
                "clt");

        response = Request.doPostRequest(Endpoint.ADD_NEW_EMPLOYEE, newEmployee);
    }

    @Then("The new employee must be create successfully")
    public void assertTheNewEmployeeWasCreated() {
        Assert.assertEquals(HttpStatus.SC_ACCEPTED, response.getStatusCode());

        Assert.assertEquals(response.getBody().jsonPath().getString("nome"), "Teste Um");
        Assert.assertEquals(response.getBody().jsonPath().getString("sexo"), "m");
        Assert.assertEquals(response.getBody().jsonPath().getString("cpf"), "012.345.789-01");
        Assert.assertEquals(response.getBody().jsonPath().getString("cargo"), "Analista de Qualidade");
        Assert.assertEquals(response.getBody().jsonPath().getString("admissao"), "06/11/2020");
        Assert.assertEquals(response.getBody().jsonPath().getString("salario"), "5.000,00");
        Assert.assertEquals(response.getBody().jsonPath().getString("comissao"), "0,00");
        Assert.assertEquals(response.getBody().jsonPath().getString("tipoContratacao"), "clt");
    }

    @Given("A GET request is sent to the server with an employee's ID as parameter")
    public void listAnEmployeeById() {
        Map<String,Object> pathParams = new HashMap<>();
        pathParams.put("empregadoId", 5000);

        response = Request.doGetRequestWithPathParams(Endpoint.LIST_AN_EMPLOYEE, pathParams);
    }

    @Then("The employee whose ID was provided must be returned")
    public void assertTheNewEmployeeCouldBeFound() {
        Assert.assertEquals(HttpStatus.SC_ACCEPTED, response.getStatusCode());
    }

    @Given("A Update request is sent to the server for updating an employee")
    public void updateInformationOfTheEmployee() {
        Map<String,Object> pathParams = new HashMap<>();
        pathParams.put("empregadoId", 5001);

        Employee employee = new Employee(
                "06/11/2020",
                "Analista de Qualidade II",
                "100,00",
                "012.345.789-01",
                6,
                "Teste Um",
                "10.000,00",
                "m",
                "clt");

        response = Request.doPutRequestWithPathParams(Endpoint.UPDATE_AN_EMPLOYEE, pathParams, employee);
    }

    @Then("The employee must be updated successfully")
    public void assertTheEmployeeWasUpdated() {
        Assert.assertEquals(HttpStatus.SC_ACCEPTED, response.getStatusCode());

        Assert.assertEquals(response.getBody().jsonPath().getString("nome"), "Teste Um");
        Assert.assertEquals(response.getBody().jsonPath().getString("sexo"), "m");
        Assert.assertEquals(response.getBody().jsonPath().getString("cpf"), "012.345.789-01");
        Assert.assertEquals(response.getBody().jsonPath().getString("cargo"), "Analista de Qualidade II");
        Assert.assertEquals(response.getBody().jsonPath().getString("admissao"), "06/11/2020");
        Assert.assertEquals(response.getBody().jsonPath().getString("salario"), "10.000,00");
        Assert.assertEquals(response.getBody().jsonPath().getString("comissao"), "100,00");
        Assert.assertEquals(response.getBody().jsonPath().getString("tipoContratacao"), "clt");
    }
}
