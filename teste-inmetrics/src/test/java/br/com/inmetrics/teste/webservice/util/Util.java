package br.com.inmetrics.teste.webservice.util;

import io.restassured.authentication.PreemptiveBasicAuthScheme;

public class Util {
    public static PreemptiveBasicAuthScheme apiAuthentication(String username, String password) {
        PreemptiveBasicAuthScheme scheme = new PreemptiveBasicAuthScheme();

        scheme.setUserName(username);
        scheme.setPassword(password);

        return scheme;
    }
}
