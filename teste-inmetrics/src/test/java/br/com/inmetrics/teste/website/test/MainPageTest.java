package br.com.inmetrics.teste.website.test;

import br.com.inmetrics.teste.website.model.Employee;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static br.com.inmetrics.teste.website.constants.Constants.*;

public class MainPageTest {
    BaseSetup baseSetup;
    Employee employee;

    public MainPageTest(BaseSetup baseSetup) {
        this.baseSetup = baseSetup;

        employee = new Employee(
                "Novo Analista de Qualidade",
                "210.143.620-59",
                "m",
                "07112020",
                "Analista de Qualidade",
                "2.500,00",
                "clt"
        );
    }

    @Given("There is a new employee to be added to the system")
    public void createAnNewEmployee() {
        baseSetup.loginPage.login("usuario_numero_um", "123456");

        baseSetup.mainPage.clickOnNewEmployeeButton();
    }

    @When("All the information about the employee and the button for confirming is clicked")
    public void createNewEmployee() {
        baseSetup.addNewEmployeePage.addNewEmployee(
            employee.getName(),
            employee.getCpf(),
            employee.getGender(),
            employee.getAdmissionDate(),
            employee.getRole(),
            employee.getPayment(),
            employee.getTypeOfContract());
    }

    @Then("A new user must be added to the system")
    public void showSuccessMessageNewEmployeeAdded() {
        Assert.assertTrue(baseSetup.mainPage.getAlertSuccessText().contains(SUCCESS_MESSAGE_NEW_EMPLOYEE_ADDED));
    }

    @Given("An employee that was already added to the system")
    public void findTheEmployeeToUpdate() {
        baseSetup.loginPage.login("usuario_numero_um", "123456");
        baseSetup.mainPage.searchForAnEmployee("Novo Analista de Qualidade");
    }

    @When("The information to be updated is provide and the button to confirm is clicked")
    public void updateTheEmployee() {
        baseSetup.mainPage.clickOnTheEditButtonOfAnEmployee(0);

        employee.setName("Agora Analista de Qualidade II");
        employee.setPayment("3.500,00");

        baseSetup.addNewEmployeePage.typeOnNameField(employee.getName());
        baseSetup.addNewEmployeePage.typeOnPaymentField(employee.getPayment());
        baseSetup.addNewEmployeePage.confirm();
    }

    @Then("The user must be updated successfully")
    public void showSuccessMessageEmployeeUpdated() {
        Assert.assertTrue(baseSetup.mainPage.getAlertSuccessText().contains(SUCCESS_MESSAGE_EMPLOYEE_UPDATED));
    }

    @Given("An employee that was added to the system")
    public void findTheEmployeeToDelete() {
        baseSetup.loginPage.login("usuario_numero_um", "123456");
        baseSetup.mainPage.searchForAnEmployee("Agora Analista de Qualidade II");
    }

    @When("The button for deleting the employee is clicked")
    public void deleteTheEmployee() {
        baseSetup.mainPage.clickOnTheDeleteButtonOfAnEmployee(0);
    }

    @Then("The user must be deleted from the system")
    public void showSuccessMessageEmployeeDeleted() {
        Assert.assertTrue(baseSetup.mainPage.getAlertSuccessText().contains(SUCCESS_MESSAGE_EMPLOYEE_DELETED));
    }
}
