Feature: CRUD Employee Requests

  @webservice
  Scenario: List All Employees
    Given A GET request is sent to the server without specifying an employee
    Then A list containing all the employee must be returned

  @webservice
  Scenario: Add a New Employee
    Given A POST request is sent to server to create a new employee
    Then The new employee must be create successfully

  @webservice
  Scenario: Get An Employee By ID
    Given A GET request is sent to the server with an employee's ID as parameter
    Then The employee whose ID was provided must be returned

  @webservice
  Scenario: Update an Employee
    Given A Update request is sent to the server for updating an employee
    Then The employee must be updated successfully
