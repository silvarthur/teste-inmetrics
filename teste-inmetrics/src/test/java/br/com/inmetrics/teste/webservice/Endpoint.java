package br.com.inmetrics.teste.webservice;

public enum Endpoint {
    LIST_ALL_EMPLOYEES("/empregado/list_all"),
    LIST_AN_EMPLOYEE("/empregado/list/{empregadoId}"),
    ADD_NEW_EMPLOYEE("/empregado/cadastrar"),
    UPDATE_AN_EMPLOYEE("/empregado/alterar/{empregadoId}"),
    DELETE_AN_EMPLOYEE("/empregado/deletar/{empregadoId}");

    String endpoint;

    Endpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
