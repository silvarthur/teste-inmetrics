package br.com.inmetrics.teste.website.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpPage extends BasePage {
    @FindBy(name = "username")
    private WebElement usernameField;

    @FindBy(name = "pass")
    private WebElement passwordField;

    @FindBy(name = "confirmpass")
    private WebElement confirmPasswordField;

    @FindBy(className = "login100-form-btn")
    private WebElement signUpButton;

    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    public void signUp(String username, String password) {
        typeOnElement(usernameField, username);
        typeOnElement(passwordField, password);
        typeOnElement(confirmPasswordField, password);

        clickOnElement(signUpButton);
    }
}
