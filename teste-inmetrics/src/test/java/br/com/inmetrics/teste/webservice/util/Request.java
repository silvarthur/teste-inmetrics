package br.com.inmetrics.teste.webservice.util;

import br.com.inmetrics.teste.webservice.Endpoint;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class Request {
    public static Response doGetRequest(Endpoint endpoint) {
        return
        get(endpoint.getEndpoint()).
        then().
            contentType(ContentType.JSON).
            extract().response();
    }

    public static Response doGetRequestWithPathParams(Endpoint endpoint,
                                                      Map<String,Object> pathParams) {
        return
        given().
            pathParams(pathParams).
        when().
            get(endpoint.getEndpoint()).
        then().
            extract().response();
    }

    public static <T> Response doPostRequest(Endpoint endpoint, T model) {
        return
        given().
            contentType(ContentType.JSON).
            body(model).
        when().
            post(endpoint.getEndpoint()).
        then().
            extract().response();
    }

    public static <T> Response doPutRequestWithPathParams(Endpoint endpoint,
                                                          Map<String,Object> pathParams, T model) {
        return
        given().
            contentType(ContentType.JSON).
            pathParams(pathParams).
            body(model).
        when().
            put(endpoint.getEndpoint()).
        then().
            extract().response();
    }
}
