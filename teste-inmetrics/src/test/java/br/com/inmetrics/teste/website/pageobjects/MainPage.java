package br.com.inmetrics.teste.website.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class MainPage extends BasePage {
    @FindBy(linkText = "Funcionários")
    private WebElement employeesButton;

    @FindBy(xpath = "//a[contains(text(),'Novo Funcionário')]")
    private WebElement newEmployeeButton;

    @FindBy(xpath = "//a[contains(text(),'Sair')]")
    private WebElement logoutButton;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchField;

    @FindAll({
        @FindBy(xpath = "//tr[contains(@class, 'odd')]"),
        @FindBy(xpath = "//tr[contains(@class, 'even')]")
    })
    private List<WebElement> listOfEmployees;

    @FindBy(css = "div[class='alert alert-success alert-dismissible fade show']")
    private WebElement alertSuccessToast;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnNewEmployeeButton() {
        clickOnElement(newEmployeeButton);
    }

    public String getAlertSuccessText() {
        return getElementText(alertSuccessToast);
    }

    public void clickOnTheDeleteButtonOfAnEmployee(int employeeIndex) {
        clickOnElement(listOfEmployees.get(employeeIndex)
                .findElement(By.id("delete-btn")));
    }

    public void clickOnTheEditButtonOfAnEmployee(int employeeIndex) {
        clickOnElement(listOfEmployees.get(employeeIndex)
                .findElement(By.xpath("//button[@class='btn btn-warning']")));
    }

    public void searchForAnEmployee(String employeeData) {
        typeOnElement(searchField, employeeData);
    }

    public void waitSearchFieldToBeVisible() {
        waitElementTobeVisible(searchField);
    }

    public WebElement getLogoutButton() {
        return logoutButton;
    }
}
