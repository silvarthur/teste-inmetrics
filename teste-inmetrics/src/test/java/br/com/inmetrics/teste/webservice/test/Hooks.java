package br.com.inmetrics.teste.webservice.test;

import br.com.inmetrics.teste.webservice.util.Util;
import io.cucumber.java.Before;
import io.restassured.RestAssured;

public class Hooks {
    @Before("@webservice")
    public static void setup(){
        RestAssured.baseURI = "https://inm-api-test.herokuapp.com";

        RestAssured.authentication = Util.apiAuthentication("inmetrics", "automacao");
    }
}
