package br.com.inmetrics.teste.website.constants;

public class Constants {
    public static final String SUCCESS_MESSAGE_NEW_EMPLOYEE_ADDED = "SUCESSO! Usuário cadastrado com sucesso";
    public static final String SUCCESS_MESSAGE_EMPLOYEE_DELETED = "SUCESSO! Funcionário removido com sucesso";
    public static final String SUCCESS_MESSAGE_EMPLOYEE_UPDATED = "SUCESSO! Informações atualizadas com sucesso";
}
