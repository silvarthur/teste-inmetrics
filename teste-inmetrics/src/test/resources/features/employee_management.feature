Feature: Employee Management

  @website
  Scenario: Adding a New Employee
    Given There is a new employee to be added to the system
    When All the information about the employee and the button for confirming is clicked
    Then A new user must be added to the system

  @website
  Scenario: Updating Employee Data
    Given An employee that was already added to the system
    When The information to be updated is provide and the button to confirm is clicked
    Then The user must be updated successfully

  @website
  Scenario: Deleting an Employee
    Given An employee that was added to the system
    When The button for deleting the employee is clicked
    Then The user must be deleted from the system