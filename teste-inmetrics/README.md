# Desafio Inmetrics

## Como Executar o Projeto

Para executar os testes o seguinte comando deve ser executado utilizando o
terminal na pasta raiz do projeto:

```mvn test```

## Reports

Após execução dos testes, o link para visualização do report será exibido no
log de execução.

Os report seguem o padrão do site https://reports.cucumber.io/. 