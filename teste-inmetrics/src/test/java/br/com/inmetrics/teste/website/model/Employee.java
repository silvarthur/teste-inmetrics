package br.com.inmetrics.teste.website.model;

public class Employee {
    String name;
    String cpf;
    String gender;
    String admissionDate;
    String role;
    String payment;
    String typeOfContract;

    public Employee(String name, String cpf, String gender,
                    String admissionDate, String role, String payment,
                    String typeOfContract) {
        this.name = name;
        this.cpf = cpf;
        this.gender = gender;
        this.admissionDate = admissionDate;
        this.role = role;
        this.payment = payment;
        this.typeOfContract = typeOfContract;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(String admissionDate) {
        this.admissionDate = admissionDate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getTypeOfContract() {
        return typeOfContract;
    }

    public void setTypeOfContract(String typeOfContract) {
        this.typeOfContract = typeOfContract;
    }
}
