package br.com.inmetrics.teste.website.test;

import br.com.inmetrics.teste.website.model.User;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class SignUpTest {
    BaseSetup baseSetup;
    User newUser;

    public SignUpTest(BaseSetup baseSetup) {
        this.baseSetup = baseSetup;
    }

    @Given("An user who wants to use the system")
    public void createNewUser() {
        newUser = new User("new_test_user", "123456");
    }

    @When("The user provide all required information and confirms")
    public void registerTheNewUser() {
        baseSetup.loginPage.clickOnSignUpButton();
        baseSetup.signUpPage.signUp(newUser.getUsername(), newUser.getPassword());
    }

    @Then("The new user must be able to login successfully")
    public void login() {
        baseSetup.loginPage.login(newUser.getUsername(), newUser.getPassword());

        Assert.assertTrue(baseSetup.mainPage.getLogoutButton().isDisplayed());
    }
}
