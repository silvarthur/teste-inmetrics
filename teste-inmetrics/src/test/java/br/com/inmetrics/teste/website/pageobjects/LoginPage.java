package br.com.inmetrics.teste.website.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    @FindBy(name = "username")
    private WebElement usernameField;

    @FindBy(name = "pass")
    private WebElement passwordField;

    @FindBy(className = "login100-form-btn")
    private WebElement loginButton;

    @FindBy(linkText = "Cadastre-se")
    public WebElement signUpButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login(String username, String password) {
        typeOnElement(usernameField, username);
        typeOnElement(passwordField, password);

        clickOnElement(loginButton);
    }

    public void clickOnSignUpButton() {
        clickOnElement(signUpButton);
    }
}
