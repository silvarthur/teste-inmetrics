package br.com.inmetrics.teste.website.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddNewEmployeePage extends BasePage {
    @FindBy(id = "inputNome")
    private WebElement nameField;

    @FindBy(id = "cpf")
    private WebElement cpfField;

    @FindBy(id = "slctSexo")
    private WebElement genderField;

    @FindBy(id = "inputAdmissao")
    private WebElement admissionField;

    @FindBy(id = "inputCargo")
    private WebElement roleField;

    @FindBy(id = "dinheiro")
    private WebElement paymentField;

    @FindBy(id = "clt")
    private WebElement cltRadioButton;

    @FindBy(id = "pj")
    private WebElement pjRadioButton;

    @FindBy(className = "cadastrar-form-btn")
    private WebElement confirmButton;

    @FindBy(name = "cancelar-form-btn")
    private WebElement cancelButton;

    public AddNewEmployeePage(WebDriver driver) {
        super(driver);
    }

    public void typeOnCpfField(String input) {
        clearElement(cpfField);
        typeOnElement(cpfField, input);
    }

    public void typeOnAdmissionField(String input) {
        clearElement(admissionField);
        typeOnElement(admissionField, input);
    }

    public void typeOnNameField(String input) {
        clearElement(nameField);
        typeOnElement(nameField, input);
    }

    public void typeOnRoleField(String input) {
        clearElement(roleField);
        typeOnElement(roleField, input);
    }

    public void typeOnPaymentField(String input) {
        clearElement(paymentField);
        typeOnElement(paymentField, input);
    }

    public void selectTypeOfContract(String option) {
        clickOnElement(option.equals("clt") ? cltRadioButton : pjRadioButton);
    }

    public void selectGender(String option) {
        if(option.equals("m")) {
            typeOnElement(genderField, "Masculino");
        } else if(option.equals("f")) {
            typeOnElement(genderField, "Feminino");
        } else {
            typeOnElement(genderField, "Indiferente");
        }
    }

    public void confirm() {
        clickOnElement(confirmButton);
    }

    public void addNewEmployee(String name, String cpf,
                               String gender, String admissionDate,
                               String role, String paymentValue,
                               String typeOfContract) {
        typeOnCpfField(cpf);
        typeOnAdmissionField(admissionDate);
        typeOnNameField(name);
        typeOnRoleField(role);
        typeOnPaymentField(paymentValue);
        selectTypeOfContract(typeOfContract);
        selectGender(gender);

        confirm();
    }
}
