package br.com.inmetrics.teste.website.test;

import br.com.inmetrics.teste.website.pageobjects.AddNewEmployeePage;
import br.com.inmetrics.teste.website.pageobjects.LoginPage;
import br.com.inmetrics.teste.website.pageobjects.MainPage;
import br.com.inmetrics.teste.website.pageobjects.SignUpPage;
import org.openqa.selenium.WebDriver;

public class BaseSetup {
    public WebDriver driver;

    public LoginPage loginPage;
    public SignUpPage signUpPage;
    public MainPage mainPage;
    public AddNewEmployeePage addNewEmployeePage;
}
